'use strict';

var bodyParser = require('body-parser');
var express = require('express');

var exec = require('child_process').exec;
var fs = require('fs');
var path = require('path');


var confPath = process.argv[2];
var baseDir = path.dirname(process.mainModule.filename);
confPath = path.resolve(baseDir, confPath);
console.log("conf.path", confPath);
var conf = JSON.parse(fs.readFileSync(confPath, 'utf8'));

function puts(error, stdout, stderr) {
    console.log('stdout: ' + stdout);
    console.log('stderr: ' + stderr);
    if (error !== null) {
        console.log('exec error: ' + error);
    }
}

 
// use in your express app 
var app = express();
app.use(bodyParser.json()); // must use bodyParser in express 


app.get('/', function (req, res) {
  res.send(conf);
});

app.post('/webhook', function (req, res) {
  var commands = conf[req.body.object_kind];

  if (typeof commands != 'undefined') {
    commands.forEach(function(val, index, array){
      exec("BASE_DIR='" + path.dirname(confPath) + "'; cd $BASE_DIR; " + val, puts);
    });
  }
  res.send(req.body);
});

 
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
  var baseDir = path.dirname(process.mainModule.filename);
});
